# Takione Infra
GitOps code pour déployer l'infrastructure Takione.

## Purpose
Ce contenu a été conçu à des fins de démonstration pour montrer une approche d'architecture GitOps end-to-end.

Le lien vers la conférence et le live-coding associé sera disponible prochainement.

Si vous débutez avec la stack Terraform, Ansible, Kubernetes, Helm et ArgoCD, pensez à [nous contacter](mailto:contact@takima.fr), nous accompagnons de nombreuses équipes avec de superbes formations sur le sujet.

## License
This repository and its content are distributed under the [AGPLv3 License](LICENSE.md).

Any improvements to this codebase, or any side-code integrating part of this codebase should be made available as open-source.

Need to use it for a specific closed-source project? [Let's discuss it](mailto:contact@takima.fr)

